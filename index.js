function bai_1() {
  var a = document.getElementById("b1-so-1").value * 1;
  var b = document.getElementById("b1-so-2").value * 1;
  var c = document.getElementById("b1-so-3").value * 1;
  var biggest;
  var big_second;
  var big_third;
  if (a >= b && a >= c) {
    biggest = a;
    if (b >= c) {
      big_second = b;
      big_third = c;
    } else {
      big_second = c;
      big_third = b;
    }
  } else if (b >= a && b >= c) {
    biggest = b;
    if (a >= c) {
      big_second = a;
      big_third = c;
    } else {
      big_second = c;
      big_third = a;
    }
  } else {
    biggest = c;
    if (a >= b) {
      big_second = a;
      big_third = b;
    } else {
      big_second = b;
      big_third = a;
    }
  }
  document.getElementById("b1-sap-xep").innerText =
    big_third + " < " + big_second + " < " + biggest;
}

function bai_2() {
  var thanh_vien = document.getElementById("b2-thanh-vien").value * 1;
  switch (thanh_vien) {
    case 0:
      document.getElementById("b2-loi-chao").innerText = "Chào người lạ!";
      break;
    case 1:
      document.getElementById("b2-loi-chao").innerText = "Chào bố ạ!";
      break;
    case 2:
      document.getElementById("b2-loi-chao").innerText = "Chào mẹ ạ!";
      break;
    case 3:
      document.getElementById("b2-loi-chao").innerText = "Chào anh trai!";
      break;
    case 4:
      document.getElementById("b2-loi-chao").innerText = "Chào em gái nhé!";
      break;
    default:
      document.getElementById("b2-loi-chao").innerText =
        "Chào người ngoài hành tinh!";
  }
}

function bai_3() {
  var a = document.getElementById("b3-so-1").value * 1;
  var b = document.getElementById("b3-so-2").value * 1;
  var c = document.getElementById("b3-so-3").value * 1;

  var so_chan = 0;
  var so_le = 0;

  if (a % 2 == 0) {
    so_chan++;
  } else {
    so_le++;
  }
  if (b % 2 == 0) {
    so_chan++;
  } else {
    so_le++;
  }
  if (c % 2 == 0) {
    so_chan++;
  } else {
    so_le++;
  }

  document.getElementById("b3-dem").innerText =
    "Có " + so_chan + " số chẵn, " + so_le + " số lẻ";
}

function bai_4() {
  var a = document.getElementById("b4-so-1").value * 1;
  var b = document.getElementById("b4-so-2").value * 1;
  var c = document.getElementById("b4-so-3").value * 1;

  var loai_tam_giac = "";
  if (a + b > c && b + c > a && a + c > b) {
    if (a == b && b == c && a == c) {
      loai_tam_giac = "Tam giác đều";
    } else if (a == b || b == c || a == c) {
      if (
        a * a + b * b == c * c ||
        b * b + c * c == a * a ||
        a * a + c * c == b * b
      ) {
        loai_tam_giac = "Tam giác vuông cân";
      } else {
        loai_tam_giac = "Tam giác cân";
      }
    } else if (
      a * a + b * b == c * c ||
      b * b + c * c == a * a ||
      a * a + c * c == b * b
    ) {
      loai_tam_giac = "Tam giác vuông";
    } else {
      loai_tam_giac = "Tam giác thường";
    }
  } else {
    loai_tam_giac = "Đây không phải 3 cạnh của một tam giác";
  }

  document.getElementById("b4-du-doan").innerText = loai_tam_giac;
}
